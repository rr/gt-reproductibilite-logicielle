# GT Reproductibilité logicielle

## But de ce dépôt

Dans le cadre du réseau **Recherche reproductible**, ce dépôt centralise les documents produits par la GT *reproductibilité logicielle* : CR de réunion, rapports, présentations, guides, etc. 

Il appartient au groupe GitLab "rr", dans lequel les membres peuvent créer les projets qui le souhaitent. 

## Liens utiles

- [Forum du réseau Recherche Reproductible](https://forum.recherche-reproductible.fr/)

