---
marp: true
theme: socrates
author: Pierre-Antoine Bouttier
paginate: true
footer: GT Repr. Log. - pierre-antoine.bouttier@univ-grenoble-alpes.fr
---

<!-- _class: titlepage -->

# GT Reproductibilité logicielle
## Activités du groupe

#### [Contact : Pierre-Antoine Bouttier](mailto:pierre-antoine.bouttier@univ-grenoble-alpes.fr)

---
# Slides GT

- [30/06/2023](./gt_20230630.html) - [PDF](./gt_20230630.html)